package university.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import university.demo.entity.Lector;

@Repository
public interface LectorRepository extends JpaRepository<Lector, Long>, JpaSpecificationExecutor<Lector> {
}
