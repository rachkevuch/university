package university.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import university.demo.entity.Department;
import university.demo.entity.Lector;
import university.demo.exeption.WrongInputException;
import university.demo.repository.DepartmentRepository;

import java.util.*;

import static university.demo.entity.Degree.*;

@Service
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public Department findOne(Long id) throws WrongInputException {
        return departmentRepository.findById(id).orElseThrow(WrongInputException::new);
    }

    public Integer averageSalary(Department department) {
        int sal = 0;
        sal += department.getLectorsList().stream().mapToInt(Lector::getSalary).sum();
        return sal / department.getLectorsList().size();
    }

    public Map<String, Integer> statistic(Department department) {

        int assistants = (int) department.getLectorsList().stream().filter(e -> e.getDegree().equals(ASSISTANT)).count();
        int professors = (int) department.getLectorsList().stream().filter(e -> e.getDegree().equals(ASSOCIATE_PROFESSOR)).count();
        int associateProfessors = (int) department.getLectorsList().stream().filter(e -> e.getDegree().equals(PROFESSOR)).count();

        Map<String, Integer> statistic = new HashMap<>();
        statistic.put("Assistants", assistants);
        statistic.put("Professors", professors);
        statistic.put("Associate Professors", associateProfessors);

        return statistic;
    }

    public Integer countEmployee(Department department) {
        return department.getLectorsList().size();
    }
}
