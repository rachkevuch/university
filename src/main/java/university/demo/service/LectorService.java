package university.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import university.demo.entity.Lector;
import university.demo.repository.LectorRepository;
import university.demo.specification.LectorSpecefication;
import java.util.ArrayList;
import java.util.List;


@Service
public class LectorService {

    @Autowired
    private LectorRepository lectorRepository;

    public List<Lector> findByFilter(String filter) {
        List<Lector> lectors = new ArrayList<>();
        lectorRepository.findAll(new LectorSpecefication(filter)).forEach(e -> {
            Lector lector = new Lector(e.getId(), e.getName(), e.getDegree(), e.getSalary());
            lectors.add(lector);

        });

        return lectors;
    }
}
