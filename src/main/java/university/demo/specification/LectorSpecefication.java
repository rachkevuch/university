package university.demo.specification;

import org.springframework.data.jpa.domain.Specification;
import university.demo.entity.Lector;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class LectorSpecefication implements Specification<Lector> {

    private String filter;

    public LectorSpecefication(String filter) {
        this.filter = filter;
    }


    @Override
    public Predicate toPredicate(Root<Lector> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        Predicate byNameLike = findByNameLike(root, criteriaBuilder);
        if (byNameLike != null) predicates.add(byNameLike);

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }


    private Predicate findByNameLike(Root<Lector> root, CriteriaBuilder criteriaBuilder) {
        if (filter == null || filter.trim().isEmpty()) {
            return null;
        }
        return criteriaBuilder.like(root.get("name"), '%' + filter + '%');
    }
}