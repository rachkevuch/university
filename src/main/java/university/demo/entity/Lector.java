package university.demo.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor

@Entity
public class Lector {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Degree degree;

    private Integer salary;

    @ManyToMany
    private List<Department> departmentsList = new ArrayList<>();

    public Lector(Long id, String name, Degree degree, Integer salary) {
        this.id =id;
        this.name = name;
        this.degree = degree;
        this.salary = salary;
    }
}
