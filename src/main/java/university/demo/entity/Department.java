package university.demo.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString

@Entity
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String headName;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "departmentsList")
    private List<Lector> lectorsList = new ArrayList<>();

}
