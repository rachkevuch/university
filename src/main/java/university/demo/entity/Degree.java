package university.demo.entity;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString

public enum Degree {

    ASSISTANT,
    ASSOCIATE_PROFESSOR,
    PROFESSOR

}
