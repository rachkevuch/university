package university.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import university.demo.entity.Department;
import university.demo.entity.Lector;
import university.demo.exeption.WrongInputException;
import university.demo.repository.DepartmentRepository;
import university.demo.repository.LectorRepository;
import university.demo.service.DepartmentService;
import university.demo.service.LectorService;
import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Map;

import static university.demo.entity.Degree.ASSISTANT;
import static university.demo.entity.Degree.PROFESSOR;

@SpringBootApplication
public class DemoApplication {


    private static final BufferedReader READER = new BufferedReader(new InputStreamReader(System.in));

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private LectorService lectorService;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private LectorRepository lectorRepository;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }


    @PostConstruct
    public void init() throws IOException, WrongInputException {
//        recordDataToDatabase();

        System.out.println("\n" +
                " _   _ _   _   _                     __  \n" +
                "| | | (_) | | | |                    \\ \\ \n" +
                "| |_| |_  | |_| |__   ___ _ __ ___  (_) |\n" +
                "|  _  | | | __| '_ \\ / _ \\ '__/ _ \\   | |\n" +
                "| | | | | | |_| | | |  __/ | |  __/  _| |\n" +
                "\\_| |_/_|  \\__|_| |_|\\___|_|  \\___| (_) |\n" +
                "                                     /_/ \n" +
                "                                         \n");

        while (true) {
            System.out.println();
            System.out.println("If you want to work with departments enter \"1\".");
            System.out.println("To global search enter \"2\".");
            System.out.println("To exit enter \"0\".");
            switch (READER.readLine()) {
                case "1":
                    System.out.println("There is a list of departments, please select one:");
                    departmentRepository.findAll().forEach(e->{
                        System.out.print(e.getId()+": ");
                        System.out.println(e.getName());
                    });
                    System.out.println("Enter the number: ");
                    Long id = Long.valueOf(READER.readLine());

                    if (departmentRepository.findById(id).isPresent()) {
                        System.out.println("1: Find out who is head of department " + departmentService.findOne(id).getName());
                        System.out.println("2: Show " + departmentService.findOne(id).getName() + " statistic");
                        System.out.println("3: Show the average salary for department " + departmentService.findOne(id).getName());
                        System.out.println("4: Show count of employee");
                        System.out.println("0: Exit");
                        switch (READER.readLine()) {
                            case "1":
                                System.out.println(departmentService.findOne(id).getHeadName() + " is head of Department");
                                break;
                            case "2":
                                Iterator iterator = departmentService.statistic(departmentService.findOne(id)).entrySet().iterator();
                                while (iterator.hasNext()) {
                                    Map.Entry statistic = (Map.Entry)iterator.next();
                                    System.out.println(statistic.getKey() + " = " + statistic.getValue());
                                }
                                break;
                            case "3":
                                System.out.println("The average salary on this department is " +
                                        departmentService.averageSalary(departmentService.findOne(id)));
                                break;
                            case "4":
                                System.out.println("Number of employees on " + departmentService.findOne(id).getName() +
                                        " department: " + departmentService.countEmployee(departmentService.findOne(id)));
                                break;
                            case "0":
                                System.out.println("Bye");
                                return;
                            default:
                                System.out.println("There is no such option");
                                break;
                        }
                    } else {
                        System.out.println("Department with id "+ id +" not exist!");
                    }
                    break;

                case "2":
                    System.out.println("Enter the name or surname of lector whom you want to find");
                    String search = READER.readLine();
                    if (!lectorService.findByFilter(search).isEmpty()) {
                        lectorService.findByFilter(search).forEach(e ->
                                System.out.println(e.getName() + ", salary: " + e.getSalary()+", degree: " + e.getDegree().name() + ";")
                        );
                    } else {
                        System.out.println("No results");
                    }

                    break;
                case "0":
                    System.out.println("Bye");
                    return;
            }
        }
    }


    public void recordDataToDatabase() {
        Department a = new Department();
        a.setName("Biomedical Engineering");
        a.setHeadName("Ivan Ivanov");

        Department b = new Department();
        b.setName("Computer Engineering");
        b.setHeadName("Vasyl Hunkalo");

        Department c = new Department();
        c.setName("Aerospace Engineering");
        c.setHeadName("Orest Kylagin");

        Department d = new Department();
        d.setName("English Literature");
        d.setHeadName("Iryna Drebot");

        departmentRepository.save(a);
        departmentRepository.save(b);
        departmentRepository.save(c);
        departmentRepository.save(d);


        Lector lector = new Lector();
        lector.setName("Petro Petrenko");
        lector.setSalary(900);
        lector.setDegree(ASSISTANT);
        lector.getDepartmentsList().add(a);
        lector.getDepartmentsList().add(b);

        Lector lector1 = new Lector();
        lector1.setName("Oleg Rybkin");
        lector1.setSalary(300);
        lector1.setDegree(PROFESSOR);
        lector1.getDepartmentsList().add(a);
        lector1.getDepartmentsList().add(b);
        lector1.getDepartmentsList().add(c);
        lector1.getDepartmentsList().add(d);

        Lector lector2 = new Lector();
        lector2.setName("Ivanka Duda");
        lector2.setSalary(200);
        lector2.setDegree(ASSISTANT);
        lector2.getDepartmentsList().add(d);

        lectorRepository.save(lector);
        lectorRepository.save(lector1);
        lectorRepository.save(lector2);
    }
}

